# Copyright 2020 Giovani Bernardes
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Launch Gazebo with a world that has Scooby, as well as the follow node."""

import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.actions import IncludeLaunchDescription
from launch.conditions import IfCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node


def generate_launch_description():

    pkg_gazebo_ros = get_package_share_directory('gazebo_ros')
    pkg_scooby_gazebo = get_package_share_directory('scooby_gazebo')
    scooby_description_pkg=get_package_share_directory('scooby_description')
     
    use_robot_state_pub = LaunchConfiguration('use_robot_state_pub')
    use_rviz = LaunchConfiguration('use_rviz')
    use_teleop = LaunchConfiguration('use_teleop')
    urdf_file= LaunchConfiguration('urdf_file')

    # Gazebo launch
    gazebo = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(pkg_gazebo_ros, 'launch', 'gazebo.launch.py'),
        )
    )

    start_robot_state_publisher_cmd = Node(
        condition=IfCondition(use_robot_state_pub),
        package='robot_state_publisher',
        executable='robot_state_publisher',
        name='robot_state_publisher',
        output='screen',
        parameters=[{'use_sim_time': True}],
        arguments=[urdf_file])

    start_scooby_teleop_cmd = Node(
        condition=IfCondition(use_teleop),
        package='scooby_teleop',
        executable='scooby_teleop',
        name='scooby_teleop',
        output='screen',
        parameters=[{'use_sim_time': True}],
        )

    rviz = Node(
        package='rviz2',
        node_executable='rviz2',
        arguments=['-d', os.path.join(scooby_description_pkg, 'rviz', 'odom_imu.rviz')],
        parameters=[{'use_sim_time': True}],
        condition=IfCondition(use_rviz)
    )
    
    odom_tf = Node(
            package='robot_localization',
            executable='ekf_node',
            name='ekf_filter_node',
            output='screen',
            parameters=[os.path.join(get_package_share_directory("scooby_localization"), 'params', 'ekf_continuous.yaml'),
          {'use_sim_time': True}],
            remappings = [('odometry/filtered', 'odometry/cont_filtered')]
           )
           
    map_tf = Node(
            package='robot_localization',
            executable='ekf_node',
            name='ekf_filter_node',
            output='screen',
            parameters=[os.path.join(get_package_share_directory("scooby_localization"), 'params', 'ekf_discrete.yaml'),
          {'use_sim_time': True}],
            remappings = [('odometry/filtered', 'odometry/discr_filtered')]
           )

    sim_gps = Node(
            package='robot_localization',
            executable='ekf_node',
            name='ekf_filter_node',
            output='screen',
            parameters=[os.path.join(get_package_share_directory("scooby_localization"), 'params', 'sim_gps.yaml'),
          {'use_sim_time': True}],
            remappings = [('odometry/filtered', 'Real_pose')]
           )
     
    odom_node = Node(
        package='scooby_localization',
        executable='odom_exec',
        name='odometry_node',
        output='screen',
        parameters=[os.path.join(get_package_share_directory("scooby_localization"), 'params', 'odometry.yaml'),
        {'use_sim_time': True}]
        )
        
    odom_imu = Node(
            package='robot_localization',
            executable='ekf_node',
            name='ekf_filter_node',
            output='screen',
            parameters=[os.path.join(get_package_share_directory("scooby_localization"), 'params', 'odom_imu.yaml'),
          {'use_sim_time': True}],
            remappings = [('odometry/filtered', 'Odom_imu')]
           )
        
    return LaunchDescription([
        DeclareLaunchArgument(
          'world',
          default_value=[os.path.join(pkg_scooby_gazebo, 'worlds', 'scooby_empty.world'), ''],
          description='SDF world file'),
        DeclareLaunchArgument('use_rviz', default_value='true',
                              description='Open RViz.'),
        DeclareLaunchArgument('use_robot_state_pub', default_value='true',
                              description='Open robot state publisher'),
        DeclareLaunchArgument('use_teleop', default_value='true',
                              description='Open scooby_teleop'),      
        DeclareLaunchArgument('urdf_file',default_value=os.path.join(scooby_description_pkg, 'urdf', 'scooby.urdf'),
                              description='urddeclare_urdf_cmd =f file complete path'),                   
        
        gazebo,
        map_tf,
        odom_tf,
        start_scooby_teleop_cmd,
        sim_gps,
        rviz,
        odom_node,
        odom_imu
        #errors_node
    ])
