# Scooby localization package tutorial

## Install

Assuming that the appropriate ROS2 version is already installed:

2. Install robot state publisher and robot localization package. 

        sudo apt install ros-foxy-robot-localization ros-foxy-robot-state-publisher

3. Clone Scooby:

        git clone https://Withor@bitbucket.org/Withor/kalman_tutorial_ws.git

4. Build and install:

        cd ~/ws
        colcon build

## Run

1. Setup environment variables (the order is important):

        . /usr/share/gazebo/setup.sh
        . ~/kalman_tutorial_ws/install/setup.bash

2. Launch the pure odometry simulation:

        ros2 launch scooby_gazebo odom.launch.py world:=scooby_factory_v1.world 

2. Launch the continuous (odom+imu) sensor fusion simulation:

        ros2 launch scooby_gazebo odom_imu.launch.py world:=scooby_factory_v1.world 

3. Launch the global (odom+imu+gps) sensor fusion simulation simulation:

        ros2 launch scooby_gazebo odom.launch.py world:=scooby_factory_v1.world 
  


