#ifndef ODOMETRY_ERRORS_HPP_
#define ODOMETRY_ERRORS_HPP_

#include <chrono>
#include <functional>
#include <memory>
#include <string>
#include <random>

#include "rclcpp/rclcpp.hpp"
#include <nav_msgs/msg/odometry.hpp>
#include <tf2/LinearMath/Quaternion.h>


class OdometryErrors : public rclcpp::Node
{
 public:
  explicit OdometryErrors();

 private:
  void contCallback(const nav_msgs::msg::Odometry::SharedPtr odom);
  void discrCallback(const nav_msgs::msg::Odometry::SharedPtr odom);
  void gpsCallback(const nav_msgs::msg::Odometry::SharedPtr odom);
  void timer_callback();
  void publish(float * pose, std::string type);
  double getYaw(double x, double y, double z, double w);

  rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr cont_sub;
  rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr discr_sub;
  rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr gps_sub;

  rclcpp::Time time;

  rclcpp::TimerBase::SharedPtr timer_;
  rclcpp::Publisher<nav_msgs::msg::Odometry>::SharedPtr cont_publisher_;
  rclcpp::Publisher<nav_msgs::msg::Odometry>::SharedPtr disc_publisher_;
  rclcpp::Publisher<nav_msgs::msg::Odometry>::SharedPtr gps_publisher_;

  std::shared_ptr<rclcpp::Node> nh_;

  float poseCont[3] = {0, 0, 0}; //[positionX, positionY, orientationZ, orientationW];
  float poseDisc[3] = {0, 0, 0};
  float realPose[3] = {0, 0, 0};
};

#endif 