/*******************************************************************************
* Copyright 2019 ROBOTIS CO., LTD.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*******************************************************************************/

/* Author: Darby Lim */

#include "odometry.hpp"
#include <random>

using namespace std::chrono_literals;

Odometry::Odometry()
: Node("odometry_node", rclcpp::NodeOptions().use_intra_process_comms(true)),
  wheels_separation_(0.4),
  wheels_radius_(0.1),
  use_imu_(false),
  publish_tf_(false),
  initialized(false),
  imu_angle_(0.0f)
{
  nh_ = std::shared_ptr<::rclcpp::Node>(this, [](::rclcpp::Node *) {});

  RCLCPP_INFO(nh_->get_logger(), "Init Odometry");

  nh_->declare_parameter("odometry.frame_id");
  nh_->declare_parameter("odometry.child_frame_id");
  nh_->declare_parameter("odometry.use_imu");
  nh_->declare_parameter("odometry.publish_tf");
  nh_->declare_parameter("odometry.wheels_separation");
  nh_->declare_parameter("odometry.wheels_radius");

  nh_->get_parameter_or<bool>(
    "odometry.use_imu",
    use_imu_,
    false);

  nh_->get_parameter_or<bool>(
    "odometry.publish_tf",
    publish_tf_,
    false);

  nh_->get_parameter_or<std::string>(
    "odometry.frame_id",
    frame_id_of_odometry_,
    std::string("map"));

  nh_->get_parameter_or<std::string>(
    "odometry.child_frame_id",
    child_frame_id_of_odometry_,
    std::string("chassi_link"));

  nh_->get_parameter_or<double>(
    "odometry.wheels_separation",
    wheels_separation_,
    0.3);

  nh_->get_parameter_or<double>(
    "odometry.wheels_radius",
    wheels_radius_,
    0.1);
  
  auto qos = rclcpp::QoS(rclcpp::KeepLast(10));
  odom_pub_ = nh_->create_publisher<nav_msgs::msg::Odometry>("scooby/wheel_encoders", qos);

  tf_broadcaster_ = std::make_unique<tf2_ros::TransformBroadcaster>(nh_);

  joint_state_sub_ = nh_->create_subscription<sensor_msgs::msg::JointState>(
    "joint_states",
    1,
    std::bind(&Odometry::joint_state_callback, this, std::placeholders::_1));
  
  initial_pose_sub_ = nh_->create_subscription<nav_msgs::msg::Odometry>(
      "Real_pose",
      qos,
      std::bind(&Odometry::initial_pose_callback, this, std::placeholders::_1));

}

void Odometry::initial_pose_callback(const nav_msgs::msg::Odometry::SharedPtr gps_odom)
{
  //initialized = true;
  if(initialized == false)
  {
    robot_pose_[0] = gps_odom->pose.pose.position.x;
    robot_pose_[1] = gps_odom->pose.pose.position.y;

    tf2::Quaternion q(gps_odom->pose.pose.orientation.x, gps_odom->pose.pose.orientation.y, gps_odom->pose.pose.orientation.z, gps_odom->pose.pose.orientation.w);
    
    double siny_cosp = 2 * (q.w ()* q.z() + q.x() * q.y());
    double cosy_cosp = 1 - 2 * (q.y() * q.y() + q.z() * q.z());

    robot_pose_[2] = std::atan2(cosy_cosp, siny_cosp) + M_PI/2;

    initialized = true;
  }
}

void Odometry::joint_state_callback(const sensor_msgs::msg::JointState::SharedPtr joint_state_msg)
{
  double duration = joint_state_msg->header.stamp.sec - last_time.seconds();

  update_joint_state(joint_state_msg);
  calculate_odometry(duration);
  publish(joint_state_msg->header.stamp);

  last_time = joint_state_msg->header.stamp;
}

void Odometry::publish(const rclcpp::Time & now)
{
  if(initialized)
  {
    auto odom_msg = std::make_unique<nav_msgs::msg::Odometry>();

    odom_msg->header.frame_id = frame_id_of_odometry_;
    odom_msg->child_frame_id  = child_frame_id_of_odometry_;
    odom_msg->header.stamp = now;

    odom_msg->pose.pose.position.x = robot_pose_[0];
    odom_msg->pose.pose.position.y = robot_pose_[1];
    odom_msg->pose.pose.position.z = 0;

    tf2::Quaternion q;
    q.setRPY(0.0, 0.0, robot_pose_[2]);

    odom_msg->pose.pose.orientation.x = q.x();
    odom_msg->pose.pose.orientation.y = q.y();
    odom_msg->pose.pose.orientation.z = q.z();
    odom_msg->pose.pose.orientation.w = q.w();

    odom_msg->twist.twist.linear.x  = robot_vel_[0];
    odom_msg->twist.twist.angular.z = robot_vel_[2];

    // TODO: Find more accurate covariance.
     odom_msg->pose.covariance[0] = 1e3;
     odom_msg->pose.covariance[7] = 1e3;
     odom_msg->pose.covariance[14] = 1;
     odom_msg->pose.covariance[21] = 1;
     odom_msg->pose.covariance[28] = 1;
     odom_msg->pose.covariance[35] = 0.872665;

     odom_msg->twist.covariance[0] = 0.1;
     odom_msg->twist.covariance[7] = 1;
     odom_msg->twist.covariance[14] = 1;
     odom_msg->twist.covariance[21] = 1;
     odom_msg->twist.covariance[28] = 1;
     odom_msg->twist.covariance[35] = 0.1;
    
    geometry_msgs::msg::TransformStamped odom_tf;

    odom_tf.transform.translation.x = odom_msg->pose.pose.position.x;
    odom_tf.transform.translation.y = odom_msg->pose.pose.position.y;
    odom_tf.transform.translation.z = odom_msg->pose.pose.position.z;
    odom_tf.transform.rotation      = odom_msg->pose.pose.orientation;

    odom_tf.header.frame_id = frame_id_of_odometry_;
    odom_tf.child_frame_id = child_frame_id_of_odometry_;
    odom_tf.header.stamp = now;

    odom_pub_->publish(std::move(odom_msg));

    if (publish_tf_)
      tf_broadcaster_->sendTransform(odom_tf);
  }
}

void Odometry::update_joint_state(
  const std::shared_ptr<sensor_msgs::msg::JointState const> &joint_state)
{
  double wheel_0 = joint_state->position[0];
  double wheel_1 = joint_state->position[1];

  if(last_joint_positions[0] == 0.0)
  {
    last_joint_positions[0] = wheel_0;
    last_joint_positions[1] = wheel_1;
  }

  diff_joint_positions_[0] = wheel_0 - last_joint_positions[0];
  diff_joint_positions_[1] = wheel_1 - last_joint_positions[1];

  last_joint_positions[0] = wheel_0;
  last_joint_positions[1] = wheel_1;
}

bool Odometry::calculate_odometry(double duration)
{
  auto odom_msg = std::make_unique<nav_msgs::msg::Odometry>();

  // rotation value of wheel [rad]
  double w_r = diff_joint_positions_[0];
  double w_l = diff_joint_positions_[1];
  
  double b = wheels_separation_;

  double step_time = std::abs(duration);

  double sl = w_l * wheels_radius_;
  double sr = w_r * wheels_radius_;
  double ssum = sl + sr;
  
  double sdiff = sr - sl;
  double dtheta = (sdiff) / (2.0 * b);

  double dx = ((ssum) / 2.0) * cos(robot_pose_[2] + dtheta / 2.0);
  double dy = ((ssum) / 2.0) * sin(robot_pose_[2] + dtheta/ 2.0);
  
  robot_pose_[0] += dx;
  robot_pose_[1] += dy;
  robot_pose_[2] += dtheta;

  if(step_time > 0.0)
  {
    double w = dtheta / step_time;
    double v = sqrt(dx * dx + dy * dy) / step_time;

    robot_vel_[0] = v;
    robot_vel_[1] = 0.0;
    robot_vel_[2] = w;
  }

  rclcpp::sleep_for(std::chrono::nanoseconds(75000000));
  

  return true;
}

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<Odometry>());
  rclcpp::shutdown();

  return 0;
}
